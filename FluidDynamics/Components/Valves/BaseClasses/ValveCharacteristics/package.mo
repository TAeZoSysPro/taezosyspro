within TAeZoSysPro.FluidDynamics.Components.Valves.BaseClasses;

package ValveCharacteristics "Functions for valve characteristics"
  extends Modelica.Icons.VariantsPackage;
end ValveCharacteristics;
