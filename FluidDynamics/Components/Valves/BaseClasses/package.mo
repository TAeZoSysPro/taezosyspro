within TAeZoSysPro.FluidDynamics.Components.Valves;

package BaseClasses "Base classes used in the Valves package (only of interest to build new component models)"
  extends Modelica.Icons.BasesPackage;
end BaseClasses;
