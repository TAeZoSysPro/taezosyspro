within TAeZoSysPro.HeatTransfer.Functions.ExchangerHeatTransferCoeff;

partial function baseFun "Base class for heat transfer coefficient calculation in heat exchanger"

  extends Modelica.Icons.Function ;
  //
  output Modelica.SIunits.CoefficientOfHeatTransfer h  "Heat transfer Coefficient" ;

end baseFun;
