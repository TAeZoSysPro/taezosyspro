within TAeZoSysPro.HeatTransfer.Functions;

package ExchangerHeatTransferCoeff "Package of functions to compute the heat transfer coefficient between the two fluids of an exchanger"

  extends Modelica.Icons.FunctionsPackage ;
  
end ExchangerHeatTransferCoeff;
