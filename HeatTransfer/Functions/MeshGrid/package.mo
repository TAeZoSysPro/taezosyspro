within TAeZoSysPro.HeatTransfer.Functions;

package MeshGrid "Package of functions to set the shape and the size of a mesh"

  extends Modelica.Icons.FunctionsPackage ;

end MeshGrid;
