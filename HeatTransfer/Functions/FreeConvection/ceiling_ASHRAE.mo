within TAeZoSysPro.HeatTransfer.Functions.FreeConvection;

function ceiling_ASHRAE

  extends Modelica.Icons.Function;

  input Modelica.SIunits.TemperatureDifference dT "Temperature difference plate - fluid";
  input Modelica.SIunits.RayleighNumber Ra;
  output Modelica.SIunits.NusseltNumber Nu;

protected
  //boundary Rayleigh number
  constant Real Ra_1 = 200, Ra_2 = 10 ^ 4, Ra_3 = 8 * 10 ^ 6 ;
  Modelica.SIunits.NusseltNumber Nu_up, Nu_buffer ;
  Real x_small "shift value to enter in the polynomial fitting to remove discontinuities";
  
algorithm
  // initialization
  x_small := 0 ;
  Nu_up := 0 ;
  Nu_buffer := 0 ;

  if dT < 0 then

    if Ra < Ra_1 then
      x_small:= Ra_1/100 ; 
      Nu_buffer := 0.96 * Ra ^ (1 / 6);
      Nu_up := 0.59 * Ra ^ (1 / 4);
      Nu := Modelica.Fluid.Utilities.regStep(x = Ra_1-Ra-x_small, x_small = x_small, y1 = Nu_buffer, y2 = Nu_up) ;
      
    elseif Ra >= Ra_1 and Ra < Ra_2 then
      x_small:= Ra_2/100 ; 
      Nu_buffer := 0.59 * Ra ^ (1 / 4);
      Nu_up := 0.54 * Ra ^ (1 / 4);
      Nu := Modelica.Fluid.Utilities.regStep(x = Ra_2-Ra-x_small, x_small = x_small, y1 = Nu_buffer, y2 = Nu_up) ;
      
    elseif Ra >= Ra_2 and Ra < Ra_3 then
      x_small:= Ra_3/100 ; 
      Nu_buffer := 0.54 * Ra ^ (1 / 4);
      Nu_up := 0.15 * Ra ^ (1 / 3);
      Nu := Modelica.Fluid.Utilities.regStep(x = Ra_3-Ra-x_small, x_small = x_small, y1 = Nu_buffer, y2 = Nu_up) ;
      
    elseif Ra >= Ra_3  then
      Nu := 0.15 * Ra ^ (1 / 3);
      
    end if ;
    
    assert(not(Ra < 1), "the Rayleigh number <1 is out of the range of the correlation", level = AssertionLevel.warning) ;
    assert(not(Ra > 1.5*10^9), "the Rayleigh number >1.5*10^9 is out of the range of the correlation", level = AssertionLevel.warning) ;
  
  else
  
    Nu := 0.27 * Ra ^ (1 / 4);
    assert(not(Ra < 10^5), "the Rayleigh number <10^5 is out of the range of the correlation", level = AssertionLevel.warning) ;
    assert(not(Ra > 10^10), "the Rayleigh number >10^9 is out of the range of the correlation", level = AssertionLevel.warning) ;

  end if;
  
annotation(Documentation(info = "
<html>
	<head>
	  <title>ceiling_ASHRAE</title>
	  <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">
	</head>

	<body>
	  <p>
        The following bLloyd and Moran correlation is applicable for an horizontal plate in rest environment (no forced convection) where the fluid is <b>bellow</b> the wall.  </br> 
        The formula is picked from the ASHRAE guide chapter 3 table 10 equation T10.5 to T10.9
	  </p>

	  <img src=\"modelica://TAeZoSysPro/Information/HeatTransfer/Functions/FreeConvection/EQ_ground_ASHRAE.png\" />

	  <p>
		The correlation is given for a Rayleigh number:
		  <ul>
		  <li> 10<sup>5</sup> < Ra < 10<sup>10</sup> for a Downward-facing heated plate </li>
		  <li> 1 < Ra < 10<sup>9</sup> for a Downward-facing cooled plate </li>
		  </ul>
	  </p>

	  <p>
		Where :
		<ul>
		  <li> Nu is the Nusselt number </li>
		  <li> Ra is the Rayleigh number </li>
		</ul>
	  </p>	

	</body>
</html>"),
    Diagram(coordinateSystem(grid = {1, 1})));

end ceiling_ASHRAE;
