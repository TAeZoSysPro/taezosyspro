within TAeZoSysPro.HeatTransfer.Functions;

package ExchangerEffectiveness "Package of functions to compute the effectiveness of an exchanger"

  extends Modelica.Icons.FunctionsPackage ;

end ExchangerEffectiveness;
