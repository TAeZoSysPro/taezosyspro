within TAeZoSysPro.HeatTransfer;

package Interfaces

  extends Modelica.Icons.InterfacesPackage ;

annotation(Documentation(info = "
<html>
  <p>
    This package contains the definition of 'ports' to exchange data with others modules
  </p>
</html>
"));

end Interfaces;
