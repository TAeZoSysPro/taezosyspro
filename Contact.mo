within TAeZoSysPro;

class Contact

  extends Modelica.Icons.Contact;
  annotation(
      preferredView = "info",
      Documentation(info = "<html><head>
	</head>
	
	<body>
		<div style=\"text-align: left;\">
			<b> <font face=\"Century Gothic\">Pascal Borel</font></b>
		</div>
		
		<p>
		</p>
		
		<ul><font face=\"Century Gothic\"> EDF R&amp;D  </font><p></p><font face=\"Century Gothic\">
			6 Quai Watier </font><p></p><font face=\"Century Gothic\">
			78400 Chatou  </font><p></p><font face=\"Century Gothic\">
			Email: <a href=\"mailto:pascal.borel@edf.fr\">pascal.borel@edf.fr</a> </font>
		</ul>
		
		<p <=\"\" p=\"\">
		
		<font face=\"Century Gothic\"><b>Felix Marsollier </b> </font>
		
		</p><p>
		</p>
		
		<ul><font face=\"Century Gothic\"> Kury Ingénierie </font>
		
		<p>
		</p>
		
		<font face=\"Century Gothic\">
			12 avenue Léon Cambillard </font><p></p><font face=\"Century Gothic\">
			92140 Clamart </font><p></p><font face=\"Century Gothic\">
			Email: <a href=\"mailto:felix.marsollier@kury.fr\">felix.marsollier@kury.fr</a></font> </ul><div><br></div>
		
		<p>
		</p>
			
		<dl>
			<dt>
				this Modelica package is developed and licensed under the <a href=\"modelica://Modelica.UsersGuide.ModelicaLicense2\">Modelica License 2</a> by:
			</dt>
			
			<dt> 
			</dt>
			
			<dd> EDVANCE </dd>
		</dl>

		<p>
			<b>--------------------------------------------------------------<br>
			Licensed by EDVANCE under the Modelica License 2<br>
			Copyright © EDVANCE 2016 - 2017<br>
			Author : EDVANCE (2017)<br>
			Version : 3.1.0<br>
			--------------------------------------------------------------</b>
		</p>
		
	

</body></html>"),
      Diagram(coordinateSystem(grid = {1, 2})));
  

end Contact;